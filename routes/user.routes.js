const Router = require('express');
const router = new Router();
const userController = require('../controllers/user.controller');
const authMiddleware = require('../middleware/authMiddleware');

router.get('/me', authMiddleware, userController.getUser);
router.patch('/me', authMiddleware, userController.changePassword);
router.delete('/me', authMiddleware, userController.deleteUser);


module.exports = router;